import exceptions.CustomException;

public class CustomExceptionApp {

	public static void main(String[] args) {
		System.out.println("Mensaje mostrado por pantalla");
		
		//Lanzamos la excepcion customizada con el mensaje que queremos que tenga y la recojemos mostrando un mensaje por consola seguido del mensaje de la excepcion
		try {
			throw new CustomException("Excepcion customizada");
		}catch(CustomException e) {
			System.out.println("Excepcion capturada con mensaje: " + e);
		}
		
		System.out.println("Programa terminado");
	}
}
